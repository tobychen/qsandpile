#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class QLineEdit;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void CreateBoxGrid();

    // Handle PileValue array
    int GetPile(int i, int j);
    void SetPile(int i, int j, int val);
    void AddToPile(int i, int j, int delta);
    void UpdateTile(int i, int j);
    bool IsOnEdge(int i, int j);
    float GetAverage();

    // Handle GUI updates
    void SetPileBox(int i, int j, int val);
    void UpdateVisual();

private slots:
    void on_actionAddSand_triggered();
    void on_actionAdd1Sand_triggered();

private:
    Ui::MainWindow *ui;
    int GridWidth = 17;
    int GridHeight = 17;
    QLineEdit* GridArray[100][100];
    int PileValue[100][100];
};

#endif // MAINWINDOW_H
