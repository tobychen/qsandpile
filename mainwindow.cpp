#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QLineEdit>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    CreateBoxGrid();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::CreateBoxGrid()
{
    for(int i = 0; i < GridWidth; i++)
        for(int j = 0; j < GridHeight; j++)
        {
            PileValue[i][j] = 3;
            QLineEdit* box = new QLineEdit();
            ui->boxGrid->addWidget(box, i, j);
            box->setMinimumSize(20, 20);
            box->setAlignment(Qt::AlignCenter);
            box->setReadOnly(true);
            GridArray[i][j] = box;
        }
    this->resize(0, 0);
    UpdateVisual();
}

int MainWindow::GetPile(int i, int j)
{
    return PileValue[i][j];
}

void MainWindow::SetPile(int i, int j, int val)
{
    PileValue[i][j] = val;
}

void MainWindow::AddToPile(int i, int j, int delta)
{
    SetPile(i, j, GetPile(i, j) + delta);
}

void MainWindow::UpdateTile(int i, int j)
{
    if(IsOnEdge(i, j)) return;
    while(GetPile(i, j) >= 4)
    {
        AddToPile(i, j, -4);
        AddToPile(i + 1, j, 1);
        AddToPile(i - 1, j, 1);
        AddToPile(i, j + 1, 1);
        AddToPile(i, j - 1, 1);
        UpdateTile(i + 1, j);
        UpdateTile(i - 1, j);
        UpdateTile(i, j + 1);
        UpdateTile(i, j - 1);
    }
}

bool MainWindow::IsOnEdge(int i, int j)
{
    if(i == 0 || j == 0 || i == GridWidth - 1 || j == GridWidth - 1)
        return true;
    return false;
}

float MainWindow::GetAverage()
{
    float sum = 0.f;
    for(int i = 1; i < GridWidth - 1; i++)
        for(int j = 1; j < GridHeight - 1; j++)
        {
            sum += GetPile(i, j);
        }
    float numOfPiles = (GridWidth - 2) * (GridHeight - 2);
    return sum / numOfPiles;
}

void MainWindow::SetPileBox(int i, int j, int val)
{
    GridArray[i][j]->setText(QString::number(val));
    if(IsOnEdge(i, j))
    {
        GridArray[i][j]->setStyleSheet("QLineEdit { background: rgb(150, 150, 255); }");
        return;
    }
    switch(val)
    {
    case 0:
        GridArray[i][j]->setStyleSheet("QLineEdit { background: rgb(255, 255, 255); }");
        break;
    case 1:
        GridArray[i][j]->setStyleSheet("QLineEdit { background: rgb(255, 170, 170); }");
        break;
    case 2:
        GridArray[i][j]->setStyleSheet("QLineEdit { background: rgb(255, 100, 100); }");
        break;
    case 3:
        GridArray[i][j]->setStyleSheet("QLineEdit { background: rgb(255, 0, 0); }");
        break;
    default:
        break;
    }
}

void MainWindow::UpdateVisual()
{
    for(int i = 0; i < GridWidth; i++)
        for(int j = 0; j < GridHeight; j++)
        {
            SetPileBox(i, j, PileValue[i][j]);
        }
    ui->averageSand->setText("Average: " + QString::number(GetAverage()));
}

void MainWindow::on_actionAddSand_triggered()
{
    for(int i = 0; i < 100; i++)
    {
        AddToPile(GridWidth/2, GridHeight/2, 1);
        UpdateTile(GridWidth/2, GridHeight/2);
    }
    UpdateVisual();
}

void MainWindow::on_actionAdd1Sand_triggered()
{
    AddToPile(GridWidth/2, GridHeight/2, 1);
    UpdateTile(GridWidth/2, GridHeight/2);
    UpdateVisual();
}
